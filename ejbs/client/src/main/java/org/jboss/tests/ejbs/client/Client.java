package org.jboss.tests.ejbs.client;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.jboss.tests.ejbs.a.HelloA;

public class Client {

    static String appName = "tests-ejb-a";

    public static void main(String[] args) throws Exception {
        // suppress output of client messages
        Logger.getLogger("org.jboss").setLevel(Level.OFF);
        Logger.getLogger("org.xnio").setLevel(Level.OFF);

        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        p.put(Context.PROVIDER_URL, "remote+http://localhost:8080");
        InitialContext context = new InitialContext(p);

        final String rcal = "ejb:tests-ejb-a-pom/ejb//HelloABean!" + HelloA.class.getName();
        final HelloA remote = (HelloA) context.lookup(rcal);
        final String result = remote.hello();

        System.out.println("Invoke All succeed: " + result);
        context.close();
    }

}
