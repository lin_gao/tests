# EJBs tests

## Build

> mvn clean install

## Start Server A in localhost (IP: 127.0.0.1):

> bin/standalone.sh

## Start Server B in a VM(IP: 192.168.122.2):

> bin/standalone.sh -b 0.0.0.0 -c standalone-ha.xml

## Add application remote user with username `ejbuser`:

> bin/add-user.sh -a -u ejbuser -p redhat1! -ds

## Configure Server A from localhost WildFly CLI:

> /socket-binding-group=standard-sockets/remote-destination-outbound-socket-binding=remote-ejb:add(host=192.168.122.2, port=8080)
> /subsystem=remoting/remote-outbound-connection=remote-ejb-connection-1:add(outbound-socket-binding-ref=remote-ejb,protocol=http-remoting,security-realm=ApplicationRealm,username=ejbuser)
> /subsystem=remoting/remote-outbound-connection=remote-ejb-connection-1/property=SASL_POLICY_NOANONYMOUS:add(value=false)
> /subsystem=remoting/remote-outbound-connection=remote-ejb-connection-1/property=SSL_ENABLED:add(value=false)
> /core-service=management/security-realm=ApplicationRealm/server-identity=secret:add(value="cmVkaGF0MSE=")

   ** Here the secret value is the prompted by `add-user.sh`

## Deploy in following order:

> deploy tests-ejb-b-pom.ear to server B (IP: 192.168.122.2)
> deploy ejbb/ear/target/tests-ejb-b-pom.ear

> deploy tests-ejb-a-pom.ear to server A (IP: 127.0.0.1)
> deploy ejba/ear/target/tests-ejb-a-pom.ear

## Run Client multiple times:

> cd client
> mvn exec:exec

You should always see:

"Invoke All succeed:  Hello From A  Hello From B"



