package org.jboss.tests.ejbs.a;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.jboss.tests.ejbs.b.HelloB;

@Stateless
@Remote(HelloA.class)
public class HelloABean implements HelloA, Serializable {

    private static final long serialVersionUID = 1L;

    @Resource(lookup = "ejb:tests-ejb-b-pom/ejb//HelloBBean!org.jboss.tests.ejbs.b.HelloB")
    HelloB helloB;

    @Override
    public String hello() {
        StringBuilder sb = new StringBuilder(" Hello From A ");
        sb.append(helloB.hello());
        return sb.toString();
    }
}
