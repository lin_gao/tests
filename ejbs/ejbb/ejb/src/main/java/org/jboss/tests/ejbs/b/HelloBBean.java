package org.jboss.tests.ejbs.b;

import java.io.Serializable;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless
@Remote(HelloB.class)
public class HelloBBean implements HelloB, Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public String hello() {
        return " Hello From B ";
    }
}
