package org.jboss.tests.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("pure/proxy")
public interface IRest {
    @Path("test/{a}/{b}")
    @GET
    String test(@PathParam("a") String a, @PathParam("b") String b);
}
