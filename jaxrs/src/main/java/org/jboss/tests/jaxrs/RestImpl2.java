package org.jboss.tests.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test2")
public class RestImpl2 implements IRest{

    @Path("test/{a}/{b}")
    @GET
    @Override
    public String test(String a, String b) {
        return a + " - &*%*&%*&%&^%^$&^$&^$ -  " + b;
    }

}
