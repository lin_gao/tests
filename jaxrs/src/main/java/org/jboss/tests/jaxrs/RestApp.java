package org.jboss.tests.jaxrs;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/rest")
public class RestApp extends javax.ws.rs.core.Application{

}
