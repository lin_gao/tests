package org.jboss.tests.exec;

import java.util.Map;

public class MaxPoints {

	public static void main(String[] args) {
		Point[] points = new Point[4];
		points[0] = new Point(1, 1);
		points[1] = new Point(1, 2);
		points[2] = new Point(3, 3);
		points[3] = new Point(1, 1);
		System.out.println("Max points for: " + MaxPoints.maxPoints(points));
	}

	public static int maxPoints(Point[] points) {
		if (points == null) return 0;
        int len = points.length;
        if (len <= 2) return len;
        int result = 0;
        for (int i = 0; i < len; i ++) {
            int samePoints = 0;
            int max = 0;
            Map<String, Integer> map = new java.util.HashMap<>();
            for (int j = i + 1; j < len; j ++) {
                int dx = points[j].x - points[i].x;
                int dy = points[j].y - points[i].y;
                if (dx == 0 && dy == 0) {
                    samePoints++;
                    continue;
                }
                int gcd = gcd(dx, dy);
                if (gcd != 0) {
                    dx= (int)(dx/gcd);
                    dy= (int)(dy/gcd);
                }
                String key = dx + "@" + dy;
                map.put(key, map.getOrDefault(key, 0) + 1);
                max = Math.max(max, map.get(key));
            }
            result = Math.max(result, max + samePoints + 1);
        }
        return result;
	}

    private static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a%b);
    }

	static class Point {
		private int x, y;
		Point(){this.x = 0; this.y = 0;}
		Point(int x, int y) {this.x = x; this.y = y;}
	}
}
