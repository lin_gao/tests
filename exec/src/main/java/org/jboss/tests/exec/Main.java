package org.jboss.tests.exec;

import java.net.InetAddress;

public class Main {
  public static void main(String[] args) throws Exception {
    System.out.println("Host Address: " + InetAddress.getLocalHost().getHostAddress());
    System.out.println("Host Address: " + InetAddress.getByName("rhost1").getHostAddress());
    System.out.println("Host Address: " + InetAddress.getByName("rhost2").getHostAddress());
  }
}
