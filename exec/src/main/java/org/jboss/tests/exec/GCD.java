package org.jboss.tests.exec;

public class GCD {
	public static void main(String[] args) {
        // tests for some integers
		System.out.println("GCD of (120, 90) is: " + GCD.gcd(120, 90));
		System.out.println("GCD of (90, 120) is: " + GCD.gcd(90, 120));
		System.out.println("GCD of (7, 5) is: " + GCD.gcd(7, 5));
	}

	public static final int gcd(int a, int b) {
		if (b == 0) return a;
		return gcd(b, a%b);
	}
}
